﻿namespace Shapes
{
    using System.Linq;

    /// <summary>
    ///     Base shape class
    /// </summary>
    public class Shape
    {
        /// <summary>
        ///     Alls the equal.
        /// </summary>
        /// <typeparam name="T">  </typeparam>
        /// <param name="values"> The values. </param>
        /// <returns>  </returns>
        public bool AllEqual<T>(params T[] values)
        {
            if (values == null || values.Length == 0) return true;

            return values.Distinct().Skip(1).Any();
        }

        /// <summary>
        ///     Checks if 4 values are equal
        /// </summary>
        /// <param name="number"> The number. </param>
        /// <param name="values"> The values. </param>
        /// <returns>  </returns>
        public bool FourEqual(double number, params double[] values)
        {
            var counter = 0;
            var valid = false;
            if (values == null) return true;

            foreach (var x in values)
                if (x == number)
                    counter += 1;

            if (counter >= 4) valid = true;

            return valid;
        }

        /// <summary>
        ///     Called when [equal].
        /// </summary>
        /// <param name="number"> The number. </param>
        /// <param name="values"> The values. </param>
        /// <returns>  </returns>
        public bool OneEqual(double number, params double[] values)
        {
            var valid = false;
            if (values == null) return true;

            foreach (var x in values)
                if (x == number)
                    valid = true;

            return valid;
        }

        /// <summary>
        ///     Twoes the equal.
        /// </summary>
        /// <param name="number"> The number. </param>
        /// <param name="values"> The values. </param>
        /// <returns>  </returns>
        public bool TwoEqual(double number, params double[] values)
        {
            var counter = 0;
            var valid = false;
            if (values == null) return true;

            foreach (var x in values)
                if (x == number)
                    counter += 1;

            if (counter >= 2) valid = true;

            return valid;
        }
    }
}