﻿namespace Shapes
{
    using System;

    /// <summary>
    /// A class representing a sphere
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Sphere : Shape
    {
        /// <summary>
        /// The circumference
        /// </summary>
        public double circumference;

        /// <summary>
        /// The radius
        /// </summary>
        public double radius;

        /// <summary>
        /// The sarea
        /// </summary>
        public double sarea;

        /// <summary>
        /// The volume
        /// </summary>
        public double volume;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sphere" /> class.
        /// </summary>
        /// <param name="radius"> The radius. </param>
        /// <param name="circumference"> The circumference. </param>
        /// <param name="volume"> The volume. </param>
        /// <param name="sarea"> The sarea. </param>
        /// <exception cref="ExceptionNoValuesGiven"> At least 1 value should be given </exception>
        public Sphere(double radius = 0.0d, double circumference = 0.0d, double volume = 0.0d, double sarea = 0.0d)
        {
            this.radius = radius;
            this.circumference = circumference;
            this.volume = volume;
            this.sarea = sarea;

            if (AllEqual(this.radius, this.circumference, this.volume, this.sarea, 0.0d))
            {
                CalcRadius();
                CalcCircumference();
                CalcSArea();
                CalcVolume();
            }
            else
            {
                throw new ExceptionNoValuesGiven("At least 1 value should be given");
            }
        }

        /// <summary>
        /// Calculates the circumference.
        /// </summary>
        private void CalcCircumference()
        {
            circumference = Math.PI * 2 * radius;
        }

        /// <summary>
        /// Calculates the radius.
        /// </summary>
        private void CalcRadius()
        {
            if (radius != 0.0d)
            {
                return;
            }

            if (circumference != 0.0d)
            {
                radius = circumference / Math.PI / 2;
            }
            else if (volume != 0.0d)
            {
                radius = Math.Pow(volume / (4d / 3d * Math.PI), 1d / 3d);
            }
            else if (sarea != 0.0d)
            {
                radius = Math.Pow(sarea / (4 * Math.PI), 1d / 2d);
            }
        }

        /// <summary>
        /// Calculates the surface area.
        /// </summary>
        private void CalcSArea()
        {
            sarea = 4 * Math.PI * Math.Pow(radius, 2);
        }

        /// <summary>
        /// Calculates the volume.
        /// </summary>
        private void CalcVolume()
        {
            volume = 4d / 3d * Math.PI * Math.Pow(radius, 3);
        }
    }
}