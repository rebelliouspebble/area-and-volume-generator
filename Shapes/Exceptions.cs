﻿namespace Shapes
{
    using System;

    /// <summary>
    ///     Exception for invalid values
    /// </summary>
    /// <seealso cref="System.Exception" />
    internal class ExceptionInvalidValue : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionInvalidValue" /> class.
        /// </summary>
        public ExceptionInvalidValue()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionInvalidValue" /> class.
        /// </summary>
        /// <param name="message"> The message that describes the error. </param>
        public ExceptionInvalidValue(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    ///     Exception for missing values
    /// </summary>
    /// <seealso cref="System.Exception" />
    internal class ExceptionMissingValue : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionMissingValue" /> class.
        /// </summary>
        public ExceptionMissingValue()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionMissingValue" /> class.
        /// </summary>
        /// <param name="message"> The message that describes the error. </param>
        public ExceptionMissingValue(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    ///     Exception for no values given
    /// </summary>
    /// <seealso cref="System.Exception" />
    internal class ExceptionNoValuesGiven : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionNoValuesGiven" /> class.
        /// </summary>
        public ExceptionNoValuesGiven()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionNoValuesGiven" /> class.
        /// </summary>
        /// <param name="message"> The message that describes the error. </param>
        public ExceptionNoValuesGiven(string message)
            : base(message)
        {
        }
    }
}