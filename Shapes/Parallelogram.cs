﻿namespace Shapes
{
    using System;

    /// <summary>
    ///     A class representing a parallelogram
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Parallelogram : Shape
    {
        /// <summary>
        ///     The area
        /// </summary>
        public double area;

        /// <summary>
        ///     The length
        /// </summary>
        public double length;

        /// <summary>
        ///     The perimeter
        /// </summary>
        public double perimeter;

        /// <summary>
        ///     The slant angle
        /// </summary>
        public double sangle;

        /// <summary>
        ///     The slant length
        /// </summary>
        public double slength;

        /// <summary>
        ///     The vertical height
        /// </summary>
        public double vheight;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Parallelogram" /> class.
        /// </summary>
        /// <param name="vheight"> The vheight. </param>
        /// <param name="length"> The length. </param>
        /// <param name="slength"> The slength. </param>
        /// <param name="sangle"> The sangle. </param>
        /// <param name="area"> The area. </param>
        /// <param name="perimeter"> The perimeter. </param>
        /// <exception cref="ExceptionMissingValue">
        ///     Either the size of the angle or the length of the slanted side is required or Either
        ///     length and area or length and vertical height are required
        /// </exception>
        public Parallelogram(
            double vheight = 0.0d,
            double length = 0.0d,
            double slength = 0.0d,
            double sangle = 0.0d,
            double area = 0.0d,
            double perimeter = 0.0d)
        {
            this.vheight = vheight;
            this.length = length;
            this.slength = slength;
            this.sangle = sangle;
            this.area = area;
            this.perimeter = perimeter;

            if (!this.AllEqual(0.0d, this.sangle, this.slength))
                throw new ExceptionMissingValue(
                    "Either the size of the angle or the length of the slanted side is required");

            if (this.TwoEqual(0.0d, this.length, this.vheight, this.area))
                throw new ExceptionMissingValue("Either length and area or length and vertical height are required");

            if (this.sangle == 0.0d) this.CalcSlantA();

            if (this.slength == 0.0d) this.CalcSlantL();

            if (this.length == 0.0d && this.area != 0.0d) this.CalcLength();

            this.CalcArea();
            this.CalcPerimeter();
            this.CalcVHeight();
        }

        /// <summary>
        ///     Calculates the area.
        /// </summary>
        public void CalcArea()
        {
            this.area = this.vheight * this.length;
        }

        /// <summary>
        ///     Calculates the length.
        /// </summary>
        public void CalcLength()
        {
            this.length = this.area / this.vheight;
        }

        /// <summary>
        ///     Calculates the perimeter.
        /// </summary>
        public void CalcPerimeter()
        {
            this.perimeter = this.length * 2 + this.slength * 2;
        }

        /// <summary>
        ///     Calculates the slant angle.
        /// </summary>
        public void CalcSlantA()
        {
            this.sangle = Math.Cosh(this.vheight / this.slength);
        }

        /// <summary>
        ///     Calculates the slant length.
        /// </summary>
        public void CalcSlantL()
        {
            this.slength = Math.Cos(this.sangle) * this.vheight;
        }

        /// <summary>
        ///     Calculates the vertical height.
        /// </summary>
        public void CalcVHeight()
        {
            if (this.vheight != 0.0d) return;

            if (this.area != 0.0d && this.length != 0.0d)
            {
                this.vheight = this.area / this.length;
                return;
            }

            if (this.sangle != 0.0d && this.slength != 0.0d) this.vheight = this.slength / Math.Cos(this.sangle);
        }
    }
}