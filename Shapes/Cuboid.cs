﻿namespace Shapes
{
    using System;

    /// <summary>
    ///     A class that represents a cuboid
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Cuboid : Shape
    {
        /// <summary>
        ///     The diagonal
        /// </summary>
        public double diagonal;

        /// <summary>
        ///     The height
        /// </summary>
        public double height;

        /// <summary>
        ///     The length
        /// </summary>
        public double length;

        /// <summary>
        ///     The sarea
        /// </summary>
        public double sarea;

        /// <summary>
        ///     The volume
        /// </summary>
        public double volume;

        /// <summary>
        ///     The width
        /// </summary>
        public double width;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Cuboid" /> class.
        /// </summary>
        /// <param name="volume"> The volume. </param>
        /// <param name="sarea"> The sarea. </param>
        /// <param name="length"> The length. </param>
        /// <param name="height"> The height. </param>
        /// <param name="width"> The width. </param>
        /// <param name="diagonal"> The diagonal. </param>
        /// <exception cref="ExceptionNoValuesGiven">
        ///     At least 2 values needed for length, width and height if area is given or all 3 are required
        /// </exception>
        /// <exception cref="ExceptionMissingValue">
        ///     As you don't have the length, width, and height value, a value for the volume is required
        ///     or At least 2 of height, length, and width are required
        /// </exception>
        public Cuboid(
            double volume = 0.0d,
            double sarea = 0.0d,
            double length = 0.0d,
            double height = 0.0d,
            double width = 0.0d,
            double diagonal = 0.0d)
        {
            this.volume = volume;
            this.sarea = sarea;
            this.length = length;
            this.height = height;
            this.width = width;
            this.diagonal = diagonal;

            if (!this.AllEqual(this.length, this.width, this.height, 0.0d))
                throw new ExceptionNoValuesGiven(
                    "At least 2 values needed for length, width and height if area is given or all 3 are required");

            if (this.TwoEqual(0.0d, this.length, this.width, this.height))
            {
                if (volume == 0.0d)
                    throw new ExceptionMissingValue(
                        "As you don't have the length, width, and height value, a value for the volume is required");

                this.CalcSide("length");
                this.CalcSide("height");
                this.CalcSide("width");
                this.CalcSArea();
                this.CalcDiagonal();
            }
            else if (this.OneEqual(0.0d, this.length, this.width, this.height))
            {
                throw new ExceptionMissingValue("At least 2 of height, length, and width are required");
            }
            else
            {
                this.CalcVolume();
                this.CalcSArea();
                this.CalcDiagonal();
            }
        }

        /// <summary>
        ///     Calculates the length of the diagonal.
        /// </summary>
        private void CalcDiagonal()
        {
            this.diagonal = Math.Sqrt(Math.Pow(this.length, 2) + Math.Pow(this.width, 2) + Math.Pow(this.height, 2));
        }

        /// <summary>
        ///     Calculates the surface area.
        /// </summary>
        private void CalcSArea()
        {
            this.sarea = this.height * this.length * 2 + this.height * this.width * 2 + this.length * this.width * 2;
        }

        /// <summary>
        ///     Calculates the length of the side given.
        /// </summary>
        /// <param name="side"> The side. </param>
        /// <exception cref="Exception"> invalid entry </exception>
        private void CalcSide(string side)
        {
            if (side == "length" && this.length == 0.0d) this.length = this.volume / this.width / this.height;
            else if (side == "height" && this.height == 0.0d) this.height = this.volume / this.width / this.length;
            else if (side == "width" && this.width == 0.0d) this.width = this.volume / this.length / this.height;
            else throw new Exception("invalid entry");
        }

        /// <summary>
        ///     Calculates the volume.
        /// </summary>
        private void CalcVolume()
        {
            this.volume = this.height * this.length * this.width;
        }
    }
}