﻿namespace Shapes
{
    using System;

    /// <summary>
    /// A class representing a Right Angled Prism
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class RAPrism : Shape
    {
        /// <summary>
        /// The adjacent
        /// </summary>
        public double adjacent;

        /// <summary>
        /// The angle
        /// </summary>
        public double angle;

        /// <summary>
        /// The hypotenuse
        /// </summary>
        public double hypotenuse;

        /// <summary>
        /// The length
        /// </summary>
        public double length;

        /// <summary>
        /// The opposite
        /// </summary>
        public double opposite;

        /// <summary>
        /// The otherangle
        /// </summary>
        public double otherangle;

        /// <summary>
        /// The sarea
        /// </summary>
        public double sarea;

        /// <summary>
        /// The volume
        /// </summary>
        public double volume;

        /// <summary>
        /// The anglerad
        /// </summary>
        private readonly double anglerad;

        /// <summary>
        /// Initializes a new instance of the <see cref="RAPrism" /> class.
        /// </summary>
        /// <param name="opposite"> The opposite. </param>
        /// <param name="hypotenuse"> The hypotenuse. </param>
        /// <param name="adjacent"> The adjacent. </param>
        /// <param name="angle"> The angle. </param>
        /// <param name="otherangle"> The otherangle. </param>
        /// <param name="volume"> The volume. </param>
        /// <param name="sarea"> The sarea. </param>
        /// <param name="length"> The length. </param>
        /// <exception cref="ExceptionMissingValue"> 
        /// At least 1 side is required or A value for an angle must be given or A value for the
        /// length is required
        /// </exception>
        public RAPrism(
            double opposite = 0.0d,
            double hypotenuse = 0.0d,
            double adjacent = 0.0d,
            double angle = 0.0d,
            double otherangle = 0.0d,
            double volume = 0.0d,
            double sarea = 0.0d,
            double length = 0.0d)
        {
            this.opposite = opposite;
            this.hypotenuse = hypotenuse;
            this.adjacent = adjacent;
            this.angle = angle;
            this.volume = volume;
            this.sarea = sarea;
            this.otherangle = otherangle;
            this.length = length;

            anglerad = angle * (Math.PI / 180);

            if (!OneEqual(0.0d, this.opposite, this.adjacent))
            {
                throw new ExceptionMissingValue("At least 1 side is required");
            }

            if (this.angle == 0.0d)
            {
                throw new ExceptionMissingValue("A value for an angle must be given");
            }

            if (this.length == 0.0d)
            {
                throw new ExceptionMissingValue("A value for the length is required");
            }

            CalcHypotenuse();
            CalcAdjacent();
            CalcOpposite();
            CalcVolume();
            CalcSArea();
            CalcOtherAngle();
        }

        /// <summary>
        /// Calculates the other angle.
        /// </summary>
        public void CalcOtherAngle()
        {
            if (otherangle == 0.0d)
            {
                otherangle = 180 - 90 - angle;
            }
        }

        /// <summary>
        /// Calculates the adjacent.
        /// </summary>
        private void CalcAdjacent()
        {
            if (adjacent != 0.0d)
            {
                return;
            }

            if (opposite != 0.0d)
            {
                adjacent = opposite / Math.Tan(anglerad);
            }
            else
            {
                adjacent = Math.Cos(anglerad) * hypotenuse;
            }
        }

        /// <summary>
        /// Calculates the hypotenuse.
        /// </summary>
        private void CalcHypotenuse()
        {
            if (hypotenuse != 0.0d)
            {
                return;
            }

            if (opposite != 0.0d)
            {
                hypotenuse = opposite / Math.Sin(anglerad);
            }
            else
            {
                hypotenuse = adjacent / Math.Cos(anglerad);
            }
        }

        /// <summary>
        /// Calculates the opposite.
        /// </summary>
        private void CalcOpposite()
        {
            if (opposite != 0.0d)
            {
                return;
            }

            if (adjacent != 0.0d)
            {
                opposite = Math.Tan(anglerad) * adjacent;
            }
            else
            {
                opposite = Math.Sin(anglerad) * hypotenuse;
            }
        }

        /// <summary>
        /// Calculates the s area.
        /// </summary>
        private void CalcSArea()
        {
            if (sarea == 0.0d)
            {
                sarea = adjacent * opposite / 2 * 2 + hypotenuse * length
                                                                   + adjacent * length
                                                                   + opposite * length;
            }
        }

        /// <summary>
        /// Calculates the volume.
        /// </summary>
        private void CalcVolume()
        {
            if (volume == 0.0d)
            {
                volume = adjacent * opposite / 2 * length;
            }
        }
    }
}