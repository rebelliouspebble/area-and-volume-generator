﻿namespace Shapes
{
    using System;

    /// <summary>
    /// A class that represents a Triangle
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Triangle : Shape
    {
        /// <summary>
        /// The angle a
        /// </summary>
        public double Angle_A;

        /// <summary>
        /// The angle b
        /// </summary>
        public double Angle_B;

        /// <summary>
        /// The angle c
        /// </summary>
        public double Angle_C;

        /// <summary>
        /// The area
        /// </summary>
        public double area;

        /// <summary>
        /// The perimeter
        /// </summary>
        public double perimeter;

        /// <summary>
        /// The side a
        /// </summary>
        public double Side_a;

        /// <summary>
        /// The side b
        /// </summary>
        public double Side_b;

        /// <summary>
        /// The side c
        /// </summary>
        public double Side_c;

        /// <summary>
        /// Variable representing the conversion ratio for radians to degrees
        /// </summary>
        private readonly double radConvert = Math.PI / 180d;

        /// <summary>
        /// Initializes a new instance of the <see cref="Triangle" /> class.
        /// </summary>
        /// <param name="angleA"> The angle a. </param>
        /// <param name="angleB"> The angle b. </param>
        /// <param name="angleC"> The angle c. </param>
        /// <param name="sideA"> The side a. </param>
        /// <param name="sideB"> The side b. </param>
        /// <param name="sideC"> The side c. </param>
        /// <param name="area"> The area. </param>
        /// <param name="perimeter"> The perimeter. </param>
        /// <exception cref="ExceptionMissingValue"> Three values at minimum should be given </exception>
        public Triangle(
            double angleA = 0.0d,
            double angleB = 0.0d,
            double angleC = 0.0d,
            double sideA = 0.0d,
            double sideB = 0.0d,
            double sideC = 0.0d,
            double area = 0.0d,
            double perimeter = 0.0d)
        {
            Angle_A = angleA;
            Angle_B = angleB;
            Angle_C = angleC;
            Side_a = sideA;
            Side_b = sideB;
            Side_c = sideC;
            this.area = area;
            this.perimeter = perimeter;

            if (FourEqual(0.0d, Angle_A, Angle_B, Angle_C, Side_a, Side_b, Side_c))
            {
                throw new ExceptionMissingValue("Three values at minimum should be given");
            }

            Solve();
            CalcPerimeter();
            CalcArea();
        }

        /// <summary>
        /// Calculates the area.
        /// </summary>
        public void CalcArea()
        {
            area = 1d / 2d * Side_a * Side_b * Math.Cos(Angle_C * radConvert)
                        / radConvert;
        }

        /// <summary>
        /// Calculates the perimeter.
        /// </summary>
        public void CalcPerimeter()
        {
            perimeter = Side_a + Side_b + Side_c;
        }

        /// <summary>
        /// Cosine Rule 1
        /// </summary>
        /// <param name="a"> a. </param>
        /// <param name="b"> The b. </param>
        /// <param name="c"> The c. </param>
        /// <returns>  </returns>
        public double CosRule1(double a, double b, double c)
        {
            // Return angle
            return Math.Acos((Math.Pow(b, 2) + Math.Pow(c, 2) - Math.Pow(a, 2)) / (2 * b * c)) / radConvert;
        }

        /// <summary>
        /// Cosine Rule 2
        /// </summary>
        /// <param name="a"> a. </param>
        /// <param name="b"> The b. </param>
        /// <param name="A1"> The a1. </param>
        /// <returns>  </returns>
        public double CosRule2(double a, double b, double A1)
        {
            // Return side
            return Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2) - 2 * a * b * Math.Cos(A1 * radConvert));
        }

        /// <summary>
        /// Sine Rule 1
        /// </summary>
        /// <param name="a"> a. </param>
        /// <param name="b"> The b. </param>
        /// <param name="A1"> The a1. </param>
        /// <returns>  </returns>
        public double SinRule1(double a, double b, double A1)
        {
            // Return angle
            return Math.Asin(b * Math.Sin(A1 * radConvert) / a) / radConvert;
        }

        /// <summary>
        /// Sine Rule 2
        /// </summary>
        /// <param name="a"> a. </param>
        /// <param name="A1"> The a1. </param>
        /// <param name="A2"> The a2. </param>
        /// <returns>  </returns>
        public double SinRule2(double a, double A1, double A2)
        {
            // Return side
            return a * Math.Sin(A2 * radConvert) / Math.Sin(A1 * radConvert);
        }

        /// <summary>
        /// Solves this instance.
        /// </summary>
        private void Solve()
        {
            if (!AllEqual(Side_a, Side_b, Side_c))
            {
                Angle_A = CosRule1(Side_a, Side_b, Side_c);
                Angle_B = CosRule1(Side_b, Side_a, Side_c);
                Angle_C = CosRule1(Side_c, Side_a, Side_b);
            }
            else if ((Side_a != 0) & (Side_b != 0) & (Angle_A != 0))
            {
                // abA = 2 sides and angle A
                Angle_B = SinRule1(Side_a, Side_b, Angle_A);
                Angle_C = 180 - Angle_A - Angle_B;
                Side_c = SinRule2(Side_a, Angle_A, Angle_C);
            }
            else if ((Side_a != 0) & (Side_b != 0) & (Angle_B != 0))
            {
                // abB = 2 sides and angle B
                Angle_A = SinRule1(Side_b, Side_a, Angle_B);
                Angle_C = 180 - Angle_A - Angle_B;
                Side_c = SinRule2(Side_a, Angle_A, Angle_C);
            }
            else if ((Side_a != 0) & (Side_b != 0) & (Angle_C != 0))
            {
                // abC = 2 sides and angle C
                Side_c = CosRule2(Side_a, Side_b, Angle_C);
                Angle_A = SinRule1(Side_c, Side_a, Angle_C);
                Angle_B = 180 - Angle_A - Angle_C;
            }
            else if ((Side_a != 0) & (Side_c != 0) & (Angle_A != 0))
            {
                // acA = 2 sides and angle A
                Angle_C = SinRule1(Side_a, Side_c, Angle_A);
                Angle_B = 180 - Angle_A - Angle_C;
                Side_b = SinRule2(Side_a, Angle_A, Angle_B);
            }
            else if ((Side_a != 0) & (Side_c != 0) & (Angle_B != 0))
            {
                // acB = 2 sides and angle B
                Side_b = CosRule2(Side_a, Side_c, Angle_B);
                Angle_C = SinRule1(Side_b, Side_c, Angle_B);
                Angle_A = 180 - Angle_B - Angle_C;
            }
            else if ((Side_a != 0) & (Side_c != 0) & (Angle_C != 0))
            {
                // acC = 2 sides and angle C
                Angle_A = SinRule1(Side_c, Side_a, Angle_C);
                Angle_B = 180 - Angle_A - Angle_C;
                Side_b = SinRule2(Side_a, Angle_A, Angle_B);
            }
            else if ((Side_b != 0) & (Side_c != 0) & (Angle_A != 0))
            {
                // bcA = 2 sides and angle A
                Side_a = CosRule2(Side_b, Side_c, Angle_A);
                Angle_B = SinRule1(Side_a, Side_b, Angle_A);
                Angle_C = 180 - Angle_A - Angle_B;
            }
            else if ((Side_b != 0) & (Side_c != 0) & (Angle_B != 0))
            {
                // bcB = 2 sides and angle B
                Angle_C = SinRule1(Side_b, Side_c, Angle_B);
                Angle_A = 180 - Angle_B - Angle_C;
                Side_a = SinRule2(Side_c, Angle_C, Angle_A);
            }
            else if ((Side_b != 0) & (Side_c != 0) & (Angle_C != 0))
            {
                // bcC = 2 sides and angle C
                Angle_B = SinRule1(Side_c, Side_b, Angle_C);
                Angle_A = 180 - Angle_B - Angle_C;
                Side_a = SinRule2(Side_c, Angle_C, Angle_A);
            }
            else if ((Angle_A != 0) & (Angle_B != 0) & (Side_a != 0))
            {
                // ABa = 2 angles and side a
                Angle_C = 180 - Angle_A - Angle_B;
                Side_b = SinRule2(Side_a, Angle_A, Angle_B);
                Side_c = SinRule2(Side_b, Angle_B, Angle_C);
            }
            else if ((Angle_A != 0) & (Angle_B != 0) & (Side_b != 0))
            {
                // ABb = 2 angles and side b
                Angle_C = 180 - Angle_A - Angle_B;
                Side_c = SinRule2(Side_b, Angle_B, Angle_C);
                Side_a = SinRule2(Side_b, Angle_B, Angle_A);
            }
            else if ((Angle_A != 0) & (Angle_B != 0) & (Side_c != 0))
            {
                // ABc = 2 angles and side c
                Angle_C = 180 - Angle_A - Angle_B;
                Side_a = SinRule2(Side_c, Angle_C, Angle_A);
                Side_b = SinRule2(Side_c, Angle_C, Angle_B);
            }
            else if ((Angle_A != 0) & (Angle_C != 0) & (Side_a != 0))
            {
                // ACa = 2 angles and side a
                Angle_B = 180 - Angle_A - Angle_C;
                Side_b = SinRule2(Side_a, Angle_A, Angle_B);
                Side_c = SinRule2(Side_a, Angle_A, Angle_C);
            }
            else if ((Angle_A != 0) & (Angle_C != 0) & (Side_b != 0))
            {
                // ACb = 2 angles and side b
                Angle_B = 180 - Angle_A - Angle_C;
                Side_c = SinRule2(Side_b, Angle_B, Angle_C);
                Side_a = SinRule2(Side_c, Angle_C, Angle_A);
            }
            else if ((Angle_A != 0) & (Angle_C != 0) & (Side_c != 0))
            {
                // ACc = 2 angles and side c
                Angle_B = 180 - Angle_A - Angle_C;
                Side_a = SinRule2(Side_c, Angle_C, Angle_A);
                Side_b = SinRule2(Side_c, Angle_C, Angle_B);
            }
            else if ((Angle_B != 0) & (Angle_C != 0) & (Side_a != 0))
            {
                // BCa = 2 angles and side a
                Angle_A = 180 - Angle_B - Angle_C;
                Side_b = SinRule2(Side_a, Angle_A, Angle_B);
                Side_c = SinRule2(Side_a, Angle_A, Angle_C);
            }
            else if ((Angle_B != 0) & (Angle_C != 0) & (Side_b != 0))
            {
                // BCb = 2 angles and side b
                Angle_A = 180 - Angle_B - Angle_C;
                Side_a = SinRule2(Side_b, Angle_B, Angle_A);
                Side_c = SinRule2(Side_a, Angle_A, Angle_C);
            }
            else if ((Angle_B != 0) & (Angle_C != 0) & (Side_c != 0))
            {
                // BCc = 2 angles and side c
                Angle_A = 180 - Angle_B - Angle_C;
                Side_b = SinRule2(Side_c, Angle_C, Angle_B);
                Side_a = SinRule2(Side_b, Angle_B, Angle_A);
            }
        }
    }
}