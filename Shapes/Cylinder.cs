﻿namespace Shapes
{
    using System;

    /// <summary>
    ///     Cylinder class
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Cylinder : Shape
    {
        /// <summary>
        ///     The circumference
        /// </summary>
        public double circumference;

        /// <summary>
        ///     The diameter
        /// </summary>
        public double diameter;

        /// <summary>
        ///     The length
        /// </summary>
        public double length;

        /// <summary>
        ///     The radius
        /// </summary>
        public double radius;

        /// <summary>
        ///     The sarea
        /// </summary>
        public double sarea;

        /// <summary>
        ///     The volume
        /// </summary>
        public double volume;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Cylinder" /> class.
        /// </summary>
        /// <param name="radius"> The radius. </param>
        /// <param name="circumference"> The circumference. </param>
        /// <param name="diameter"> The diameter. </param>
        /// <param name="length"> The length. </param>
        /// <param name="volume"> The volume. </param>
        /// <param name="sarea"> The sarea. </param>
        /// <exception cref="ExceptionMissingValue"> Length value required </exception>
        /// <exception cref="Exception"> Calculation falied, check values entered </exception>
        /// <exception cref="ExceptionNoValuesGiven"> No values given </exception>
        public Cylinder(
            double radius = 0.0d,
            double circumference = 0.0d,
            double diameter = 0.0d,
            double length = 0.0d,
            double volume = 0.0d,
            double sarea = 0.0d)
        {
            this.radius = radius;
            this.circumference = circumference;
            this.diameter = diameter;
            this.volume = volume;
            this.sarea = sarea;
            this.length = length;

            if (length == 0.0d) throw new ExceptionMissingValue("Length value required");

            var counter = 0;
            if (this.AllEqual(this.radius, this.diameter, this.circumference, this.volume, this.length, 0.0d))
            {
                while (this.radius == 0.0d || this.circumference == 0.0d || this.diameter == 0.0d
                       || this.volume == 0.0d)
                {
                    this.CalcRadius();
                    this.CalcCircumference();
                    this.CalcDiameter();
                    this.CalcVolume();
                    counter += 1;
                }

                if (counter == 5) throw new Exception("Calculation falied, check values entered");

                // Calculates the surface area
                this.sarea = Math.Pow(this.radius, 2) * Math.PI * 2 + this.circumference * this.length;
            }
            else
            {
                throw new ExceptionNoValuesGiven("No values given");
            }
        }

        /// <summary>
        ///     Calculates the circumference.
        /// </summary>
        private void CalcCircumference()
        {
            if (this.circumference != 0.0d)
            {
            }
            else if (this.diameter != 0.0d)
            {
                this.circumference = this.diameter * Math.PI;
            }
            else if (this.radius != 0.0d)
            {
                this.circumference = this.radius * 2 * Math.PI;
            }
            else
            {
                this.circumference = Math.Sqrt(this.volume / this.length / Math.PI) * 2 * Math.PI;
            }
        }

        /// <summary>
        ///     Calculates the diameter.
        /// </summary>
        private void CalcDiameter()
        {
            if (this.diameter != 0.0d)
            {
            }

            // from radius
            else if (this.radius != 0.0d)
            {
                this.diameter = this.radius * 2;
            }

            // from circumference
            else if (this.circumference != 0.0d)
            {
                this.diameter = this.circumference / Math.PI;
            }

            // from volume
            else
            {
                this.radius = Math.Sqrt(this.volume / this.length / Math.PI) * 2;
            }

            // from surface area
        }

        /// <summary>
        ///     Calculates the radius.
        /// </summary>
        private void CalcRadius()
        {
            if (this.radius != 0.0d)
            {
            }
            else if (this.diameter != 0.0d)
            {
                this.radius = this.diameter / 2;
            }
            else if (this.circumference != 0.0d)
            {
                this.radius = this.circumference / Math.PI / 2;
            }
            else
            {
                this.radius = Math.Sqrt(this.volume / this.length / Math.PI);
            }
        }

        /// <summary>
        ///     Calculates the volume.
        /// </summary>
        private void CalcVolume()
        {
            if (this.volume != 0.0d)
            {
            }
            else if (this.circumference != 0.0d)
            {
                this.volume = Math.Pow(this.circumference / Math.PI / 2, 2) * Math.PI * this.length;
            }
            else if (this.diameter != 0.0d)
            {
                this.volume = Math.Pow(this.diameter / 2, 2) * Math.PI * this.length;
            }
            else
            {
                this.volume = Math.Pow(this.radius, 2) * Math.PI * this.length;
            }
        }
    }
}