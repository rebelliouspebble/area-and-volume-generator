﻿namespace Shapes
{
    using System;

    /// <summary>
    ///     A class representing a Right Angled Triangle
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class RATriangle : Shape
    {
        /// <summary>
        ///     The adjacent
        /// </summary>
        public double adjacent;

        /// <summary>
        ///     The angle
        /// </summary>
        public double angle;

        /// <summary>
        ///     The area
        /// </summary>
        public double area;

        /// <summary>
        ///     The hypotenuse
        /// </summary>
        public double hypotenuse;

        /// <summary>
        ///     The opposite
        /// </summary>
        public double opposite;

        /// <summary>
        ///     The otherangle
        /// </summary>
        public double otherangle;

        /// <summary>
        ///     The perimeter
        /// </summary>
        public double perimeter;

        /// <summary>
        ///     The anglerad
        /// </summary>
        private readonly double anglerad;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RATriangle" /> class.
        /// </summary>
        /// <param name="opposite"> The opposite. </param>
        /// <param name="hypotenuse"> The hypotenuse. </param>
        /// <param name="adjacent"> The adjacent. </param>
        /// <param name="angle"> The angle. </param>
        /// <param name="otherangle"> The otherangle. </param>
        /// <param name="area"> The area. </param>
        /// <param name="perimeter"> The perimeter. </param>
        /// <exception cref="ExceptionMissingValue">
        ///     At least 1 side is required or A value for an angle must be given
        /// </exception>
        public RATriangle(
            double opposite = 0.0d,
            double hypotenuse = 0.0d,
            double adjacent = 0.0d,
            double angle = 0.0d,
            double otherangle = 0.0d,
            double area = 0.0d,
            double perimeter = 0.0d)
        {
            this.opposite = opposite;
            this.hypotenuse = hypotenuse;
            this.adjacent = adjacent;
            this.angle = angle;
            this.area = area;
            this.perimeter = perimeter;
            this.otherangle = otherangle;

            this.anglerad = angle * (Math.PI / 180);

            if (!this.OneEqual(0.0d, this.opposite, this.adjacent))
                throw new ExceptionMissingValue("At least 1 side is required");

            if (this.angle == 0.0d) throw new ExceptionMissingValue("A value for an angle must be given");

            this.CalcHypotenuse();
            this.CalcAdjacent();
            this.CalcOpposite();
            this.CalcArea();
            this.CalcPerimeter();
            this.CalcOtherAngle();
        }

        /// <summary>
        ///     Calculates the other angle.
        /// </summary>
        public void CalcOtherAngle()
        {
            if (this.otherangle == 0.0d) this.otherangle = 180 - 90 - this.angle;
        }

        /// <summary>
        ///     Calculates the perimeter.
        /// </summary>
        public void CalcPerimeter()
        {
            if (this.perimeter == 0.0d) this.perimeter = this.adjacent + this.opposite + this.hypotenuse;
        }

        /// <summary>
        ///     Calculates the adjacent.
        /// </summary>
        private void CalcAdjacent()
        {
            if (this.adjacent != 0.0d) return;

            if (this.opposite != 0.0d) this.adjacent = this.opposite / Math.Tan(this.anglerad);
            else this.adjacent = Math.Cos(this.anglerad) * this.hypotenuse;
        }

        /// <summary>
        ///     Calculates the area.
        /// </summary>
        private void CalcArea()
        {
            if (this.area == 0.0d) this.area = this.adjacent * this.opposite / 2;
        }

        /// <summary>
        ///     Calculates the hypotenuse.
        /// </summary>
        private void CalcHypotenuse()
        {
            if (this.hypotenuse != 0.0d) return;

            if (this.opposite != 0.0d) this.hypotenuse = this.opposite / Math.Sin(this.anglerad);
            else this.hypotenuse = this.adjacent / Math.Cos(this.anglerad);
        }

        /// <summary>
        ///     Calculates the opposite.
        /// </summary>
        private void CalcOpposite()
        {
            if (this.opposite != 0.0d) return;

            if (this.adjacent != 0.0d) this.opposite = Math.Tan(this.anglerad) * this.adjacent;
            else this.opposite = Math.Sin(this.anglerad) * this.hypotenuse;
        }
    }
}