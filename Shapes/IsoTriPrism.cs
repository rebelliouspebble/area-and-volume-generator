﻿namespace Shapes
{
    using System;

    /// <summary>
    /// A Class representing an Isosceles Triangle Prism
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class IsoTriPrism : Shape
    {
        /// <summary>
        /// The baselength
        /// </summary>
        public double baselength;

        /// <summary>
        /// The length
        /// </summary>
        public double length;

        /// <summary>
        /// The surface area
        /// </summary>
        public double sarea;

        /// <summary>
        /// The slanted length
        /// </summary>
        public double slantlength;

        /// <summary>
        /// The vertical height
        /// </summary>
        public double vertheight;

        /// <summary>
        /// The volume
        /// </summary>
        public double volume;

        /// <summary>
        /// Initializes a new instance of the <see cref="IsoTriPrism" /> class.
        /// </summary>
        /// <param name="length"> The length. </param>
        /// <param name="vertheight"> The vertheight. </param>
        /// <param name="baselength"> The baselength. </param>
        /// <param name="slantlength"> The slantlength. </param>
        /// <param name="volume"> The volume. </param>
        /// <param name="sarea"> The sarea. </param>
        /// <exception cref="ExceptionMissingValue"> 
        /// Length value required or At least 2 values for the triangle are required
        /// </exception>
        public IsoTriPrism(
            double length = 0.0d,
            double vertheight = 0.0d,
            double baselength = 0.0d,
            double slantlength = 0.0d,
            double volume = 0.0d,
            double sarea = 0.0d)
        {
            this.length = length;
            this.vertheight = vertheight;
            this.baselength = baselength;
            this.slantlength = slantlength;
            this.volume = volume;
            this.sarea = sarea;

            if (length == 0.0d)
            {
                throw new ExceptionMissingValue("Length value required");
            }

            if (TwoEqual(0.0d, this.vertheight, this.baselength, this.slantlength))
            {
                throw new ExceptionMissingValue("At least 2 values for the triangle are required");
            }

            CalcBaseLength();
            CalcSlantLength();
            CalcVertHeight();
            CalcVolume();
            CalcSArea();
        }

        /// <summary>
        /// Calculates the length of the base.
        /// </summary>
        private void CalcBaseLength()
        {
            if (vertheight != 0.0d && slantlength != 0.0d)
            {
                baselength = Math.Sqrt(Math.Pow(slantlength, 2) - Math.Pow(vertheight, 2)) * 2;
            }
        }

        /// <summary>
        /// Calculates the surface area.
        /// </summary>
        private void CalcSArea()
        {
            sarea = baselength / 2 * vertheight * 2 + length * slantlength * 2
                                                                   + baselength * length;
        }

        /// <summary>
        /// Calculates the length of the slant.
        /// </summary>
        private void CalcSlantLength()
        {
            if (vertheight != 0.0d && baselength != 0.0d)
            {
                slantlength = Math.Sqrt(Math.Pow(baselength / 2d, 2) + Math.Pow(vertheight, 2));
            }
        }

        /// <summary>
        /// Calculates the height of the vertical.
        /// </summary>
        private void CalcVertHeight()
        {
            if (baselength != 0.0d && slantlength != 0.0d)
            {
                vertheight = Math.Sqrt(Math.Pow(slantlength, 2) - Math.Pow(baselength / 2d, 2));
            }
        }

        /// <summary>
        /// Calculates the volume.
        /// </summary>
        private void CalcVolume()
        {
            volume = baselength / 2 * vertheight * length;
        }
    }
}