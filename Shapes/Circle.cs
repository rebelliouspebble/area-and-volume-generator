﻿namespace Shapes
{
    using System;

    /// <summary>
    /// The circle class
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Circle : Shape
    {
        /// <summary>
        /// The area
        /// </summary>
        public double area;

        /// <summary>
        /// The circumference
        /// </summary>
        public double circumference;

        /// <summary>
        /// The diameter
        /// </summary>
        public double diameter;

        /// <summary>
        /// The radius
        /// </summary>
        public double radius;

        /// <summary>
        /// Creates a circle with any given set of values
        /// </summary>
        /// <param name="radius"> The radius. </param>
        /// <param name="circumference"> The circumference. </param>
        /// <param name="diameter"> The diameter. </param>
        /// <param name="area"> The area. </param>
        /// <exception cref="Exception"> Calculation falied, check values entered </exception>
        /// <exception cref="ExceptionNoValuesGiven"> No values given </exception>
        public Circle(double radius = 0.0d, double circumference = 0.0d, double diameter = 0.0d, double area = 0.0d)
        {
            this.radius = radius;
            this.circumference = circumference;
            this.diameter = diameter;
            this.area = area;

            int counter = 0;
            if (AllEqual(this.radius, this.diameter, this.circumference, this.area, 0.0d))
            {
                while (this.radius == 0.0d || this.circumference == 0.0d || this.diameter == 0.0d || this.area == 0.0d)
                {
                    CalcRadius();
                    CalcArea();
                    CalcCircumference();
                    CalcDiameter();
                    counter += 1;
                }

                if (counter == 5)
                {
                    throw new Exception("Calculation falied, check values entered");
                }
            }
            else
            {
                throw new ExceptionNoValuesGiven("No values given");
            }
        }

        /// <summary>
        /// Calculates the area.
        /// </summary>
        private void CalcArea()
        {
            if (area != 0.0d)
            {
                return;
            }

            if (circumference != 0.0d)
            {
                area = Math.Pow(circumference / Math.PI / 2, 2) * Math.PI;
            }
            else if (diameter != 0.0d)
            {
                area = Math.Pow(diameter / 2, 2) * Math.PI;
            }
            else
            {
                area = Math.Pow(radius, 2) * Math.PI;
            }
        }

        /// <summary>
        /// Calculates the circumference.
        /// </summary>
        private void CalcCircumference()
        {
            if (circumference != 0.0d)
            {
            }
            else if (diameter != 0.0d)
            {
                circumference = diameter * Math.PI;
            }
            else if (radius != 0.0d)
            {
                circumference = radius * 2 * Math.PI;
            }
            else
            {
                circumference = Math.Sqrt(area / Math.PI) * 2 * Math.PI;
            }
        }

        /// <summary>
        /// Calculates the diameter.
        /// </summary>
        private void CalcDiameter()
        {
            if (diameter != 0.0d)
            {
                return;
            }

            if (radius != 0.0d)
            {
                diameter = radius * 2;
            }
            else if (circumference != 0.0d)
            {
                diameter = circumference / Math.PI;
            }
            else
            {
                diameter = Math.Sqrt(area / Math.PI) * 2;
            }
        }

        /// <summary>
        /// Calculates the radius.
        /// </summary>
        private void CalcRadius()
        {
            if (radius != 0.0d)
            {
            }
            else if (diameter != 0.0d)
            {
                radius = diameter / 2;
            }
            else if (circumference != 0.0d)
            {
                radius = circumference / Math.PI / 2;
            }
            else
            {
                radius = Math.Sqrt(area / Math.PI);
            }
        }
    }
}