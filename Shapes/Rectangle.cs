﻿namespace Shapes
{
    using System;

    /// <summary>
    ///     A class representing a Rectangle
    /// </summary>
    /// <seealso cref="Shapes.Shape" />
    public class Rectangle : Shape
    {
        /// <summary>
        ///     The area
        /// </summary>
        public double area;

        /// <summary>
        ///     The diagonal
        /// </summary>
        public double diagonal;

        /// <summary>
        ///     The height
        /// </summary>
        public double height;

        /// <summary>
        ///     The length
        /// </summary>
        public double length;

        /// <summary>
        ///     The perimeter
        /// </summary>
        public double perimeter;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Rectangle" /> class.
        /// </summary>
        /// <param name="length"> The length. </param>
        /// <param name="height"> The height. </param>
        /// <param name="diagonal"> The diagonal. </param>
        /// <param name="area"> The area. </param>
        /// <param name="perimeter"> The perimeter. </param>
        /// <exception cref="ExceptionMissingValue">
        ///     At least the length or width is required or If either length or height are given, at
        ///     least 1 other value is required
        /// </exception>
        public Rectangle(
            double length = 0.0d,
            double height = 0.0d,
            double diagonal = 0.0d,
            double area = 0.0d,
            double perimeter = 0.0d)
        {
            this.length = length;
            this.height = height;
            this.diagonal = diagonal;
            this.area = area;
            this.perimeter = perimeter;

            if (!this.AllEqual(this.length, this.height, 0.0d))
                throw new ExceptionMissingValue("At least the length or width is required");

            if (this.OneEqual(0.0d, this.length, this.height))
                if (!this.AllEqual(diagonal, area, perimeter, 0.0d))
                    throw new ExceptionMissingValue(
                        "If either length or height are given, at least 1 other value is required ");

            this.CalcLength();
            this.CalcHeight();
            this.CalcDiagonal();
            this.CalcArea();
            this.CalcPerimeter();
            this.CalcPerimeter();
        }

        /// <summary>
        ///     Calculates the area.
        /// </summary>
        private void CalcArea()
        {
            if (this.area == 0.0d) this.area = this.height * this.length;
        }

        /// <summary>
        ///     Calculates the diagonal.
        /// </summary>
        private void CalcDiagonal()
        {
            if (this.diagonal != 0.0d) return;

            if (this.height != 0.0d && this.length != 0.0d)
                this.diagonal = Math.Sqrt(Math.Pow(this.height, 2) + Math.Pow(this.length, 2));
        }

        /// <summary>
        ///     Calculates the height.
        /// </summary>
        private void CalcHeight()
        {
            if (this.height != 0.0d) return;

            if (this.area != 0.0d && this.length != 0.0d) this.length = this.area / this.length;
            else if (this.perimeter != 0.0d && this.length != 0.0d)
                this.length = (this.perimeter - this.length * 2) / 2;
            else if (this.diagonal != 0.0d && this.length != 0.0d)
                this.length = Math.Sqrt(Math.Pow(this.diagonal, 2) - Math.Pow(this.length, 2));
        }

        /// <summary>
        ///     Calculates the length.
        /// </summary>
        private void CalcLength()
        {
            if (this.length != 0.0d) return;

            if (this.area != 0.0d && this.height != 0.0d) this.length = this.area / this.height;
            else if (this.perimeter != 0.0d && this.height != 0.0d)
                this.length = (this.perimeter - this.height * 2) / 2;
            else if (this.diagonal != 0.0d && this.height != 0.0d)
                this.length = Math.Sqrt(Math.Pow(this.diagonal, 2) - Math.Pow(this.height, 2));
        }

        /// <summary>
        ///     Calculates the perimeter.
        /// </summary>
        private void CalcPerimeter()
        {
            if (this.perimeter == 0.0d) this.perimeter = this.height * 2 + this.length * 2;
            else return;
        }
    }
}