while True:
    shapename = input("Enter shape name:")
    shapeident = "txt"+shapename
    shapelower = shapename.lower()

    space = input("is shape 2d or 3d? ")


    dictname = shapelower+"Dictionary"

    vars = input("Enter the variable names and the corresponding textbox name ending (seperate the pairs with a ; and the name and textbox name with ,)  e.g. hypotenuse,hyp;")

    varsdict={}
    for i in vars.split(";"):
        name, boxname = i.split(",")
        varsdict[name] = boxname

    print('Dictionary<string, double> '+dictname+' = new Dictionary<string, double>();\n'+dictname+' = getValuesDictionary("'+shapeident+'");\ntry\n{')
    print(shapename + " " + shapelower + " = new " + shapename + "(")
    for name, boxname in varsdict.items():
            print(name + ": " + dictname + '["' + shapeident + boxname + '"],')

    if space == "2d":
        print("area: areaDouble,")
        print("perimeter: perimeterDouble);")
    elif space == "3d":
        print("volume: areaDouble,")
        print("sarea: perimeterDouble);")

    for name, boxname in varsdict.items():
        print(shapeident + boxname + ".Text = " + shapelower + "." + name + ".ToString();")

    if space == "2d":
        print("txtArea.Text = " + shapelower + ".area.ToString();")
        print("txtPerimeter.Text = " + shapelower + ".perimeter.ToString();")
    elif space == "3d":
        print("txtArea.Text = " + shapelower + ".volume.ToString();")
        print("txtPerimeter.Text = " + shapelower + ".sarea.ToString();")

    print("}\ncatch (Exception exc)\n{\nMessageBox.Show(exc.Message);\n}")
