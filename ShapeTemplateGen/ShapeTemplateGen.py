classname = input("Enter class name: ")
variables = []
varinput = input("Enter variable names, putting a ';' between each name:  ")
for i in varinput.split(";"):
    variables.append(i)

for i in variables:
    print("public double "+ i + " = 0.0d;")

stringinst = ""

for i in variables:
    stringinst = stringinst + ("double " + i + " = 0.0d,")

stringinst = stringinst[:-1]

print("public " + classname + "(" + stringinst + ")\n{")

for i in variables:
    print("this." + i + " = " + i + ";")

print("}")