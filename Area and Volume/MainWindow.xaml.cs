﻿namespace Area_and_Volume
{
    using Shapes;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class MainWindow : Window
    {
        /// <summary>
        /// List of all images in window
        /// </summary>
        private readonly List<Image> images = new List<Image>();

        /// <summary>
        /// List of all labels in window
        /// </summary>
        private readonly List<Label> labels = new List<Label>();

        /// <summary>
        /// List of all text boxes in window
        /// </summary>
        private readonly List<TextBox> txtBoxes = new List<TextBox>();

        /// <summary>
        /// Calculated status
        /// </summary>
        private bool calculated;

        /// <summary>
        /// The current shape
        /// </summary>
        private string currentShape;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            foreach (TextBox tb in FindVisualChildren<TextBox>(mainGrid))
            {
                txtBoxes.Add(tb);
            }

            foreach (Label lbl in FindVisualChildren<Label>(mainGrid))
            {
                labels.Add(lbl);
            }

            foreach (Image img in FindVisualChildren<Image>(mainGrid))
            {
                images.Add(img);
            }

            HideAll();
        }

        /// <summary>
        /// Sets hidden visibility as single var
        /// </summary>
        /// <value> Visibility.Hidden </value>
        public Visibility Hidden { get; } = Visibility.Hidden;

        /// <summary>
        /// Sets visible visibility as single var
        /// </summary>
        /// <value> Visibility.Visible </value>
        public Visibility Shown { get; } = Visibility.Visible;

        /// <summary>
        /// Finds every instance of an element e.g. all text boxes
        /// </summary>
        /// <typeparam name="T">  </typeparam>
        /// <param name="depObj"> The dep object. </param>
        /// <returns>  </returns>
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj)
            where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCalculate control.
        /// </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e"> 
        /// The <see cref="RoutedEventArgs" /> instance containing the event data.
        /// </param>
        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {
            double areaDouble;
            double perimeterDouble;
            if (txtArea.Text != string.Empty)
            {
                areaDouble = double.Parse(txtArea.Text);
            }
            else
            {
                areaDouble = 0.0d;
            }

            if (txtPerimeter.Text != string.Empty)
            {
                perimeterDouble = double.Parse(txtPerimeter.Text);
            }
            else
            {
                perimeterDouble = 0.0d;
            }

            switch (currentShape)
            {
                case "2dCircle":
                    Dictionary<string, double> circleDictionary = new Dictionary<string, double>();
                    circleDictionary = getValuesDictionary("txtCircle");
                    try
                    {
                        Circle circle = new Circle(
                            circleDictionary["txtCircleRadius"],
                            circleDictionary["txtCircleCircumference"],
                            circleDictionary["txtCircleDiameter"],
                            areaDouble);
                        txtCircleCircumference.Text = circle.circumference.ToString();
                        txtCircleDiameter.Text = circle.diameter.ToString();
                        txtCircleRadius.Text = circle.radius.ToString();
                        txtArea.Text = circle.area.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "3dCuboid":
                    Dictionary<string, double> cuboidDictionary = new Dictionary<string, double>();
                    cuboidDictionary = getValuesDictionary("txtCuboid");
                    try
                    {
                        Cuboid cuboid = new Cuboid(
                            areaDouble,
                            perimeterDouble,
                            cuboidDictionary["txtCuboidLength"],
                            cuboidDictionary["txtCuboidHeight"],
                            cuboidDictionary["txtCuboidWidth"],
                            cuboidDictionary["txtCuboidDiagonal"]);
                        txtCuboidLength.Text = cuboid.length.ToString();
                        txtCuboidDiagonal.Text = cuboid.diagonal.ToString();
                        txtCuboidHeight.Text = cuboid.height.ToString();
                        txtCuboidWidth.Text = cuboid.height.ToString();
                        txtArea.Text = cuboid.volume.ToString();
                        txtPerimeter.Text = cuboid.sarea.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "3dCylinder":
                    Dictionary<string, double> cylinderDictionary = new Dictionary<string, double>();
                    cylinderDictionary = getValuesDictionary("txtCylinder");
                    try
                    {
                        Cylinder cylinder = new Cylinder(
                            cylinderDictionary["txtCylinderRadius"],
                            cylinderDictionary["txtCylinderCircumference"],
                            cylinderDictionary["txtCylinderDiameter"],
                            cylinderDictionary["txtCylinderHeight"],
                            areaDouble,
                            perimeterDouble);
                        txtCylinderCircumference.Text = cylinder.circumference.ToString();
                        txtCylinderDiameter.Text = cylinder.diameter.ToString();
                        txtCylinderHeight.Text = cylinder.length.ToString();
                        txtCylinderRadius.Text = cylinder.radius.ToString();
                        txtArea.Text = cylinder.volume.ToString();
                        txtPerimeter.Text = cylinder.sarea.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "2dParallelogram":
                    Dictionary<string, double> parallelogramDictionary = new Dictionary<string, double>();
                    parallelogramDictionary = getValuesDictionary("txtParallelogram");
                    try
                    {
                        Parallelogram parallelogram = new Parallelogram(
                            parallelogramDictionary["txtParallelogramVertHeight"],
                            parallelogramDictionary["txtParallelogramLength"],
                            parallelogramDictionary["txtParallelogramSlantLength"],
                            parallelogramDictionary["txtParallelogramAngle"],
                            areaDouble,
                            perimeterDouble);
                        txtParallelogramAngle.Text = parallelogram.sangle.ToString();
                        txtParallelogramLength.Text = parallelogram.length.ToString();
                        txtParallelogramSlantLength.Text = parallelogram.slength.ToString();
                        txtParallelogramVertHeight.Text = parallelogram.vheight.ToString();
                        txtArea.Text = parallelogram.area.ToString();
                        txtPerimeter.Text = parallelogram.perimeter.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "3dRAPrism":
                    Dictionary<string, double> raprismDictionary = new Dictionary<string, double>();
                    raprismDictionary = getValuesDictionary("txtRAPrism");
                    try
                    {
                        double angle;
                        double otherangle;
                        double adjacent;
                        double opposite;
                        bool anglea;
                        if (raprismDictionary["txtRAPrismAngleA"] != 0.0d)
                        {
                            angle = raprismDictionary["txtRAPrismAngleA"];
                            otherangle = raprismDictionary["txtRAPrismAngleB"];
                            adjacent = raprismDictionary["txtRAPrismBase"];
                            opposite = raprismDictionary["txtRAPrismHeight"];
                            anglea = true;
                        }
                        else
                        {
                            angle = raprismDictionary["txtRAPrismAngleB"];
                            otherangle = raprismDictionary["txtRAPrismAngleA"];
                            adjacent = raprismDictionary["txtRAPrismHeight"];
                            opposite = raprismDictionary["txtRAPrismBase"];
                            anglea = false;
                        }

                        RAPrism raprism = new RAPrism(
                            hypotenuse: raprismDictionary["txtRAPrismHyp"],
                            opposite: opposite,
                            adjacent: adjacent,
                            angle: angle,
                            otherangle: otherangle,
                            length: raprismDictionary["txtRAPrismLength"],
                            volume: areaDouble,
                            sarea: perimeterDouble);
                        if (anglea)
                        {
                            txtRAPrismBase.Text = raprism.adjacent.ToString();
                            txtRAPrismHeight.Text = raprism.opposite.ToString();
                            txtRAPrismAngleA.Text = raprism.angle.ToString();
                            txtRAPrismAngleB.Text = raprism.otherangle.ToString();
                        }
                        else
                        {
                            txtRAPrismBase.Text = raprism.opposite.ToString();
                            txtRAPrismHeight.Text = raprism.adjacent.ToString();
                            txtRAPrismAngleA.Text = raprism.otherangle.ToString();
                            txtRAPrismAngleB.Text = raprism.angle.ToString();
                        }

                        txtRAPrismHyp.Text = raprism.hypotenuse.ToString();
                        txtRAPrismLength.Text = raprism.length.ToString();
                        txtArea.Text = raprism.volume.ToString();
                        txtPerimeter.Text = raprism.sarea.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "2dRATriangle":
                    Dictionary<string, double> ratriangleDictionary = new Dictionary<string, double>();
                    ratriangleDictionary = getValuesDictionary("txtRATriangle");
                    try
                    {
                        double angle;
                        double otherangle;
                        double adjacent;
                        double opposite;
                        bool anglea;
                        if (ratriangleDictionary["txtRATriangleAngleA"] != 0.0d)
                        {
                            angle = ratriangleDictionary["txtRATriangleAngleA"];
                            otherangle = ratriangleDictionary["txtRATriangleAngleB"];
                            adjacent = ratriangleDictionary["txtRATriangleBase"];
                            opposite = ratriangleDictionary["txtRATriangleHeight"];
                            anglea = true;
                        }
                        else
                        {
                            angle = ratriangleDictionary["txtRATriangleAngleB"];
                            otherangle = ratriangleDictionary["txtRATriangleAngleA"];
                            adjacent = ratriangleDictionary["txtRATriangleHeight"];
                            opposite = ratriangleDictionary["txtRATriangleBase"];
                            anglea = false;
                        }

                        RATriangle ratriangle = new RATriangle(
                            opposite,
                            ratriangleDictionary["txtRATriangleHyp"],
                            adjacent,
                            angle,
                            otherangle,
                            areaDouble,
                            perimeterDouble);

                        if (anglea)
                        {
                            txtRATriangleHeight.Text = ratriangle.opposite.ToString();
                            txtRATriangleBase.Text = ratriangle.adjacent.ToString();
                            txtRATriangleAngleA.Text = ratriangle.angle.ToString();
                            txtRATriangleAngleB.Text = ratriangle.otherangle.ToString();
                        }
                        else
                        {
                            txtRATriangleHeight.Text = ratriangle.adjacent.ToString();
                            txtRATriangleBase.Text = ratriangle.opposite.ToString();
                            txtRATriangleAngleA.Text = ratriangle.otherangle.ToString();
                            txtRATriangleAngleB.Text = ratriangle.angle.ToString();
                        }

                        txtRATriangleHyp.Text = ratriangle.hypotenuse.ToString();
                        txtArea.Text = ratriangle.area.ToString();
                        txtPerimeter.Text = ratriangle.perimeter.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "2dRectangle":
                    Dictionary<string, double> rectangleDictionary = new Dictionary<string, double>();
                    rectangleDictionary = getValuesDictionary("txtRectangle");
                    try
                    {
                        Rectangle rectangle = new Rectangle(
                            rectangleDictionary["txtRectangleLength"],
                            rectangleDictionary["txtRectangleHeight"],
                            rectangleDictionary["txtRectangleDiagonal"],
                            areaDouble,
                            perimeterDouble);
                        txtRectangleLength.Text = rectangle.length.ToString();
                        txtRectangleHeight.Text = rectangle.height.ToString();
                        txtRectangleDiagonal.Text = rectangle.diagonal.ToString();
                        txtArea.Text = rectangle.area.ToString();
                        txtPerimeter.Text = rectangle.perimeter.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "3dSphere":
                    Dictionary<string, double> sphereDictionary = new Dictionary<string, double>();
                    sphereDictionary = getValuesDictionary("txtSphere");
                    try
                    {
                        Sphere sphere = new Sphere(
                            sphereDictionary["txtSphereRadius"],
                            sphereDictionary["txtSphereCircumference"],
                            areaDouble,
                            perimeterDouble);
                        txtSphereRadius.Text = sphere.radius.ToString();
                        txtSphereCircumference.Text = sphere.circumference.ToString();
                        txtArea.Text = sphere.volume.ToString();
                        txtPerimeter.Text = sphere.sarea.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "2dSquare":
                    Dictionary<string, double> squareDictionary = new Dictionary<string, double>();
                    squareDictionary = getValuesDictionary("txtSquare");
                    try
                    {
                        Rectangle square = new Rectangle(
                            squareDictionary["txtSquareSide"],
                            squareDictionary["txtSquareSide"],
                            squareDictionary["txtSquareDiagonal"],
                            areaDouble,
                            perimeterDouble);
                        txtSquareSide.Text = square.length.ToString();
                        txtSquareDiagonal.Text = square.diagonal.ToString();
                        txtArea.Text = square.area.ToString();
                        txtPerimeter.Text = square.perimeter.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "3dTriPrism":
                    Dictionary<string, double> triprismDictionary = new Dictionary<string, double>();
                    triprismDictionary = getValuesDictionary("txtTriPrism");
                    try
                    {
                        IsoTriPrism triprism = new IsoTriPrism(
                            triprismDictionary["txtTriPrismLength"],
                            triprismDictionary["txtTriPrismVertHeight"],
                            slantlength: triprismDictionary["txtTriPrismSlantLength"],
                            baselength: triprismDictionary["txtTriPrismBase"],
                            volume: areaDouble,
                            sarea: perimeterDouble);
                        txtTriPrismLength.Text = triprism.length.ToString();
                        txtTriPrismVertHeight.Text = triprism.vertheight.ToString();
                        txtTriPrismSlantLength.Text = triprism.slantlength.ToString();
                        txtTriPrismBase.Text = triprism.baselength.ToString();
                        txtArea.Text = triprism.volume.ToString();
                        txtPerimeter.Text = triprism.sarea.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                case "2dTriangle":
                    Dictionary<string, double> triangleDictionary = new Dictionary<string, double>();
                    triangleDictionary = getValuesDictionary("txtTriangle");
                    try
                    {
                        Triangle triangle = new Triangle(
                            triangleDictionary["txtTriangleAngleA"],
                            triangleDictionary["txtTriangleAngleB"],
                            triangleDictionary["txtTriangleAngleC"],
                            triangleDictionary["txtTriangleSideA"],
                            triangleDictionary["txtTriangleSideB"],
                            triangleDictionary["txtTriangleSideC"],
                            areaDouble,
                            perimeterDouble);
                        txtTriangleAngleA.Text = triangle.Angle_A.ToString();
                        txtTriangleAngleB.Text = triangle.Angle_B.ToString();
                        txtTriangleAngleC.Text = triangle.Angle_C.ToString();
                        txtTriangleSideA.Text = triangle.Side_a.ToString();
                        txtTriangleSideB.Text = triangle.Side_b.ToString();
                        txtTriangleSideC.Text = triangle.Side_c.ToString();
                        txtArea.Text = triangle.area.ToString();
                        txtPerimeter.Text = triangle.perimeter.ToString();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                    break;

                default:
                    break;
            }

            calculated = true;
        }

        /// <summary>
        /// Handles the Click event of the btnReset control.
        /// </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e"> 
        /// The <see cref="RoutedEventArgs" /> instance containing the event data.
        /// </param>
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(mainGrid))
            {
                tb.Text = string.Empty;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the cmboShapeType control.
        /// </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e"> 
        /// The <see cref="SelectionChangedEventArgs" /> instance containing the event data.
        /// </param>
        private void cmboShapeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HideAll();
            string shapeType = cmboShapeType.SelectedItem.ToString();
            txtArea.Visibility = Shown;
            lblArea.Visibility = Shown;
            txtPerimeter.Visibility = Shown;
            lblPerimeter.Visibility = Shown;

            switch (shapeType)
            {
                case "Circle":
                    currentShape = "2dCircle";
                    imgCircle.Visibility = Shown;
                    txtCircleCircumference.Visibility = Shown;
                    txtCircleDiameter.Visibility = Shown;
                    txtCircleRadius.Visibility = Shown;
                    lblCircleCircumference.Visibility = Shown;
                    lblCircleDiameter.Visibility = Shown;
                    lblCircleRadius.Visibility = Shown;
                    txtPerimeter.Visibility = Hidden;
                    lblPerimeter.Visibility = Hidden;
                    break;

                case "Cuboid":
                    currentShape = "3dCuboid";
                    imgCuboid.Visibility = Shown;
                    txtCuboidWidth.Visibility = Shown;
                    txtCuboidDiagonal.Visibility = Shown;
                    txtCuboidHeight.Visibility = Shown;
                    txtCuboidLength.Visibility = Shown;
                    lblCuboidWidth.Visibility = Shown;
                    lblCuboidDiagonal.Visibility = Shown;
                    lblCuboidHeight.Visibility = Shown;
                    lblCuboidLength.Visibility = Shown;
                    break;

                case "Cylinder":
                    currentShape = "3dCylinder";
                    imgCylinder.Visibility = Shown;
                    txtCylinderDiameter.Visibility = Shown;
                    txtCylinderHeight.Visibility = Shown;
                    txtCylinderRadius.Visibility = Shown;
                    txtCylinderCircumference.Visibility = Shown;
                    lblCylinderDiameter.Visibility = Shown;
                    lblCylinderHeight.Visibility = Shown;
                    lblCylinderRadius.Visibility = Shown;
                    lblCylinderCircumference.Visibility = Shown;
                    txtPerimeter.IsReadOnly = true;
                    break;

                case "Parallelogram":
                    currentShape = "2dParallelogram";
                    imgParallelogram.Visibility = Shown;
                    txtParallelogramLength.Visibility = Shown;
                    txtParallelogramSlantLength.Visibility = Shown;
                    txtParallelogramVertHeight.Visibility = Shown;
                    txtParallelogramAngle.Visibility = Shown;
                    lblParallelogramLength.Visibility = Shown;
                    lblParallelogramSlantLength.Visibility = Shown;
                    lblParallelogramVertHeight.Visibility = Shown;
                    lblParallelogramAngle.Visibility = Shown;
                    break;

                case "Right Angled Prism":
                    currentShape = "3dRAPrism";
                    imgRAPrism.Visibility = Shown;
                    txtRAPrismBase.Visibility = Shown;
                    txtRAPrismHeight.Visibility = Shown;
                    txtRAPrismHyp.Visibility = Shown;
                    txtRAPrismLength.Visibility = Shown;
                    txtRAPrismAngleB.Visibility = Shown;
                    txtRAPrismAngleA.Visibility = Shown;
                    lblRAPrismBase.Visibility = Shown;
                    lblRAPrismHeight.Visibility = Shown;
                    lblRAPrismHyp.Visibility = Shown;
                    lblRAPrismLength.Visibility = Shown;
                    lblRAPrismAngleB.Visibility = Shown;
                    lblRAPrismAngleA.Visibility = Shown;
                    txtPerimeter.IsReadOnly = true;
                    txtArea.IsReadOnly = true;
                    break;

                case "Right Angled Triangle":
                    currentShape = "2dRATriangle";
                    imgRATriangle.Visibility = Shown;
                    txtRATriangleAngleA.Visibility = Shown;
                    txtRATriangleAngleB.Visibility = Shown;
                    txtRATriangleBase.Visibility = Shown;
                    txtRATriangleHeight.Visibility = Shown;
                    txtRATriangleHyp.Visibility = Shown;
                    lblRATriangleAngleA.Visibility = Shown;
                    lblRATriangleAngleB.Visibility = Shown;
                    lblRATriangleBase.Visibility = Shown;
                    lblRATriangleHeight.Visibility = Shown;
                    lblRATriangleHyp.Visibility = Shown;
                    txtPerimeter.IsReadOnly = true;
                    txtArea.IsReadOnly = true;
                    break;

                case "Rectangle":
                    currentShape = "2dRectangle";
                    imgRectangle.Visibility = Shown;
                    txtRectangleDiagonal.Visibility = Shown;
                    txtRectangleHeight.Visibility = Shown;
                    txtRectangleLength.Visibility = Shown;
                    lblRectangleDiagonal.Visibility = Shown;
                    lblRectangleHeight.Visibility = Shown;
                    lblRectangleLength.Visibility = Shown;
                    break;

                case "Sphere":
                    currentShape = "3dSphere";
                    imgSphere.Visibility = Shown;
                    txtSphereCircumference.Visibility = Shown;
                    txtSphereRadius.Visibility = Shown;
                    lblSphereCircumference.Visibility = Shown;
                    lblSphereRadius.Visibility = Shown;
                    break;

                case "Square":
                    currentShape = "2dSquare";
                    imgSquare.Visibility = Shown;
                    txtSquareDiagonal.Visibility = Shown;
                    txtSquareSide.Visibility = Shown;
                    lblSquareDiagonal.Visibility = Shown;
                    lblSquareSide.Visibility = Shown;
                    break;

                case "Triangular Prism":
                    currentShape = "3dTriPrism";
                    imgTriPrism.Visibility = Shown;
                    txtTriPrismBase.Visibility = Shown;
                    txtTriPrismLength.Visibility = Shown;
                    txtTriPrismSlantLength.Visibility = Shown;
                    txtTriPrismVertHeight.Visibility = Shown;
                    lblTriPrismBase.Visibility = Shown;
                    lblTriPrismLength.Visibility = Shown;
                    lblTriPrismSlantLength.Visibility = Shown;
                    lblTriPrismVertHeight.Visibility = Shown;
                    txtPerimeter.IsReadOnly = true;
                    txtArea.IsReadOnly = true;
                    break;

                default:
                    currentShape = "2dTriangle";
                    imgTriangle.Visibility = Shown;
                    txtTriangleAngleA.Visibility = Shown;
                    txtTriangleAngleB.Visibility = Shown;
                    txtTriangleAngleC.Visibility = Shown;
                    txtTriangleSideA.Visibility = Shown;
                    txtTriangleSideB.Visibility = Shown;
                    txtTriangleSideC.Visibility = Shown;
                    lblTriangleAngleA.Visibility = Shown;
                    lblTriangleAngleB.Visibility = Shown;
                    lblTriangleAngleC.Visibility = Shown;
                    lblTriangleSideA.Visibility = Shown;
                    lblTriangleSideB.Visibility = Shown;
                    lblTriangleSideC.Visibility = Shown;
                    txtPerimeter.IsReadOnly = true;
                    txtArea.IsReadOnly = true;
                    break;
            }

            if (currentShape[0] == '2')
            {
                lblArea.Content = "Area";
                lblPerimeter.Content = "Perimeter";
            }
            else
            {
                lblArea.Content = "Volume";
                lblPerimeter.Content = "Surface Area";
            }
        }

        /// <summary>
        /// Creates a dictionary of all values in text boxes and links them to the text box name
        /// </summary>
        /// <param name="identifier"> The shape identifier. </param>
        /// <returns>  </returns>
        private Dictionary<string, double> getValuesDictionary(string identifier)
        {
            List<TextBox> boxes = new List<TextBox>();
            IEnumerable<TextBox> query = from x in FindVisualChildren<TextBox>(mainGrid)
                                         where x.Name.StartsWith(identifier)
                                         select x;
            foreach (TextBox addBox in query)
            {
                boxes.Add(addBox);
            }

            Dictionary<string, double> valueDictionary = new Dictionary<string, double>();
            foreach (TextBox t in boxes)
            {
                if (t.Text != string.Empty)
                {
                    valueDictionary.Add(t.Name, double.Parse(t.Text));
                }
                else
                {
                    valueDictionary.Add(t.Name, 0.0d);
                }
            }

            return valueDictionary;
        }

        /// <summary>
        /// Hides all elements that change
        /// </summary>
        private void HideAll()
        {
            // Hide all text boxes
            foreach (TextBox box in txtBoxes)
            {
                box.Visibility = Hidden;
            }

            // Hide all labels
            foreach (Label lbl in labels)
            {
                lbl.Visibility = Hidden;
            }

            // Hide all images
            foreach (Image img in images)
            {
                img.Visibility = Hidden;
            }
        }

        /// <summary>
        /// Validates text entered into a textbox to only allow numbers
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> 
        /// The <see cref="TextCompositionEventArgs" /> instance containing the event data.
        /// </param>
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            if (calculated)
            {
                foreach (TextBox tb in FindVisualChildren<TextBox>(mainGrid))
                {
                    tb.Text = string.Empty;
                }

                calculated = false;
            }

            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /// <summary>
        /// Returns values as doubles in same order as input list
        /// </summary>
        /// <param name="textBoxs"> The text boxs. </param>
        /// <returns>  </returns>
        private List<double> returnValues(List<TextBox> textBoxs)
        {
            List<double> values = new List<double>();
            foreach (TextBox tb in textBoxs)
            {
                if (tb.Text == string.Empty)
                {
                    values.Add(0.0d);
                }
                else
                {
                    values.Add(double.Parse(tb.Text));
                }
            }

            return values;
        }
    }
}